<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('asesores', 'AsesorController@listarAsesor');
Route::post('asesores', 'AsesorController@agregarAsesor');
Route::put('asesores', 'AsesorController@actualizarDatosAsesor');




Route::get('administradores', 'AdministradorController@listarAdministradores');
Route::get('administradores/{administrador}', 'AdministradorController@obtenerAdministrador');
Route::post('administradores', 'AdministradorController@agregarAdministrador');
Route::put('administradores/{administrador}', 'AdministradorController@actualizarDatosAdministrador');
Route::delete('administradores/{administrador}', 'AdministradorController@eliminarDatosAdministrador');


Route::get('asesorados', 'AsesoradoController@listarAsesorado');
Route::get('asesorados/{asesorado}', 'AsesoradoController@obtenerAsesorado');
Route::post('asesorados', 'AsesoradoController@agregarAsesorado');
Route::PUT('asesorados/{asesorado}', 'AsesoradoController@actualizarDatosAsesorado');
Route::DELETE('asesorados/{asesorado}', 'AsesoradoController@eliminarDatosAsesorado');



Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');

Route::group(['middleware' => 'auth:api'], function(){
Route::post('details', 'API\UserController@details');





});
