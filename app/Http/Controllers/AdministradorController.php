<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Administrador;

class AdministradorController extends Controller
{
    public function listarAdministradores() {

        $administrador = Administrador::all();

        if($administrador->isEmpty()) {
            return response()->json(['mensaje' => 'No se encuentran administradores registrados'], 404);
        }

        return response()->json($administrador, 200);
    }

    public function obtenerAdministrador($id) {

        $administrador = Administrador::find($id);

        if(!$administrador) {
            return response()->json(['mensaje' => 'No se encontró el recurso solicitado'], 404);
        }

        return response()->json($administrador, 200);
    }


    public function agregarAdministrador(Request $request) {
        $request->validate([
            'nombre' => 'string|required',
            'contraseña' => 'string|required',
            'correo' => 'string|required',
        ]);

        $administrador = new Administrador([
            'nombre' => $request->nombre,
            'contraseña' => bcrypt($request->contraseña),
            'correo' => $request->correo,
        ]);

        $administrador->save();

        return response()->json(['mensaje' => 'Datos agregados con éxito'], 201);
    }


    public function actualizarDatosAdministrador(Request $request, $id) {
        $request->validate([
            'nombre' => 'string|required',
            'contraseña' => 'string|required',
            'correo' => 'string|required',
        ]);

        $administrador = Administrador::find($id);

        if(!$administrador) {
            return response()->json(['mensaje' => 'No se encontró el recurso solicitado'], 404);
        }

        $administrador->nombre = $request->nombre;
        $administrador->contraseña = bcrypt($request->contraseña);
        $administrador->correo = $request->correo;

        $administrador->save();

        return response()->json(['mensaje' => 'Datos actualizados con éxito']);
    }


     public function eliminarDatosAdministrador($id, Request $request){
        $administrador = Administrador::find($id);

        if (!$administrador) {
            # code...
            return response()->json(['mensaje' => 'No se encontro el recurso solicitado'], 404);
        }

        // $request->validate([
        //     'numero' => 'string|required',
        //     'nombre' => 'string|required',
        //     'contraseña' => 'string|required',
        //     'correo' => 'string|required',
        // ]);

        try {
            $administrador->delete();
            } catch(\Exception $e){
                return response()->json(['mensaje'=>$e.getMessage()], 500);
            }
            return response()->json(['mensaje'=>'Administrador eliminado'], 200);

    }
}
