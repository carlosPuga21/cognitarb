<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth; 

use Illuminate\Support\Collection as Collection;
use App\Asesorado;
use Validator;
use App\User;

class AsesoradoController extends Controller
{
    public function listarAsesorado() {
        $asesorado = Asesorado::all();

        if($asesorado->isEmpty()) {
            return response()->json(['mensaje' => 'No se encontraron asesorados registrados'], 404);
        }

        return response()->json($asesorado, 200);
    }

    public function obtenerAsesorado($id) {
        $asesorado = Asesorado::find($id);

        if(!$asesorado) {
            return response()->json(['mensaje' => 'No se encontró el recurso solicitado'], 404);
        }

        return response()->json($asesorado, 200);
    }
    public function agregarAsesorado(Request $request) {
        
        $validator = Validator::make($request->all(), [ 
            'nombre' => 'string|required',
            'ap_Paterno' => 'string|required',
            'ap_Materno' => 'string|required',
            'comprobante_pago' => 'string|required',
            'correo' => 'string|required',
            'contraseña' => 'string|required',
        ]);

        if ($validator->fails()) { 
			return response()->json(['mensaje' => 'Error al momento de validar datos'], 401);		
		}

        $asesorado = new Asesorado([
            'nombre' => $request->nombre,
            'ap_Paterno' => $request->ap_Paterno,
            'ap_Materno' => $request->ap_Materno,
            'comprobante_pago' => $request->comprobante_pago,
            'correo' => $request->correo,
            'contraseña' => bcrypt($request->contraseña),
        ]);
        

        $asesorado->save();


        $info = ['email' => $request->correo , 'name' => $request->nombre, 
        'password' => $request->contraseña ];

        $info['password'] = bcrypt($info['password']); 
        
        
        $user = User::create($info); 
        $success['token'] =  $user->createToken('MyApp')-> accessToken; 
        $success['name'] =  $user->name;
        $user->tipo_usuario = 1;
        $user->save();

        return response()->json(['mensaje' => 'Datos agregados con exito', $user], 201);
    }
    public function actualizarDatosAsesorado(Request $request, $id) {
        $request->validate([
            'nombre' => 'string|required',
            'ap_Paterno' => 'string|required',
            'ap_Materno' => 'string|required',
            'comprobante_pago' => 'string|required',
            'correo' => 'string|required',
            'contraseña' => 'string|required',
        ]);

        $asesorado= Asesorado::find($id);

        if(!$asesorado) {
            return response()->json(['mensaje' => 'No se encontró el recurso solicitado'], 404);
        }

        $asesorado->nombre = $request->nombre;
        $asesorado->ap_Paterno = $request->ap_Paterno;
        $asesorado->ap_Materno = $request->ap_Materno;
        $asesorado->comprobante_pago = $request->comprobante_pago;
        $asesorado->correo = $request->correo;
        $asesorado->contraseña = bcrypt($request->contraseña);
        $asesorado->save();

        return response()->json(['mensaje' => 'Datos actualizados con éxito'], 200);
    }
    public function eliminarDatosAsesorado($id, Request $request){
        $asesorado = Asesorado::find($id);

        if (!$asesorado) {
            # code...
            return response()->json(['mensaje' => 'No se encontro el recurso solicitado'], 404);
        }

        $request->validate([
            'nombre' => 'string|required',
            'ap_Paterno' => 'string|required',
            'ap_Materno' => 'string|required',
            'comprobante_pago' => 'string|required',
            'correo' => 'string|required',
            'contraseña' => 'string|required',
        ]);

        try {
            $asesorado->delete();
        } catch(\Exception $e){
            return response()->json(['mensaje'=>$e.getMessage()], 500);
        }
        return response()->json(['mensaje'=>'Asesorado eliminado'], 200);

    }

}
