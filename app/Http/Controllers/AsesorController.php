<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Asesor;
use App\User;
use Validator;

class AsesorController extends Controller
{
   
    public function listarAsesor() {
    	$asesor = Asesor::all();

    	if($asesor->isEmpty()) {
    		return response()->json(['mensaje' => 'No se encontraron asesores registrados'], 404);
    	}

    	return response()->json($asesor, 200);
    }

        public function obtenerAsesor($id) {
    	$asesor = Asesor::find($id);

    	if(!$asesor) {
    		return response()->json(['mensaje' => 'No se encontró el recurso solicitado'], 404);
    	}

    	return response()->json($asesor, 200);
    }
        public function agregarAsesor(Request $request) {
			$validator = Validator::make($request->all(), [ 
    		
    		'nombre' => 'string|required',
    		'ap_Paterno' => 'string|required',
    		'ap_Materno' => 'string|required',
    		'numero_tarjeta' => 'string|required',
    		'escolaridad' => 'string|required',
    		'certificacion' => 'string|required',
    		'descripcion' => 'string|required',
            'numero_tel' => 'integer|required',
    		'correo' => 'string|required',
    		'contraseña' => 'string|required',
		]);
		if ($validator->fails()) { 
			return response()->json(['mensaje' => 'Error al momento de validar datos'], 401);		
		}

    	$asesor = new Asesor([
    		'nombre' => $request->nombre,
    		'ap_Paterno' => $request->ap_Paterno,
    		'ap_Materno' => $request->ap_Materno,
    		'numero_tarjeta' => $request->numero_tarjeta,
    		'escolaridad' => $request->escolaridad,
    		'certificacion' => $request->certificacion,
    		'descripcion' => $request->descripcion,
            'numero_tel' => $request->numero_tel,
    		'correo' => $request->correo,
    		'contraseña' => bcrypt($request->contraseña),    		
    	]);

		$asesor->save();
		
			#Hacemos el enlace con el usuario y el tipo de usuario
        $info = ['email' => $request->correo , 'name' => $request->nombre, 
        'password' => $request->contraseña ];

        $info['password'] = bcrypt($info['password']); 
        
        
        $user = User::create($info); 
        $success['token'] =  $user->createToken('MyApp')-> accessToken; 
        $success['name'] =  $user->name;
        $user->tipo_usuario = 2;
        $user->save();

       
    	return response()->json(['mensaje' => 'Datos agregados con éxito',$user], 201);
    }
        public function actualizarDatosAsesor(Request $request, $id) {
    	$request->validate([
            'nombre' => 'string|required',
            'ap_Paterno' => 'string|required',
            'ap_Materno' => 'string|required',
            'numero_tarjeta' => 'string|required',
            'escolaridad' => 'string|required',
            'certificacion' => 'string|required',
            'descripcion' => 'string|required',
            'numero_tel' => 'integer|required',
            'correo' => 'string|required',
            'contraseña' => 'string|required',
    	]);

    	$asesor= Asesor::find($id);

    	if(!$asesor) {
    		return response()->json(['mensaje' => 'No se encontró el recurso solicitado'], 404);
    	}

    	$asesor->nombre = $request->nombre;
    	$asesor->ap_Paterno = $request->ap_Paterno;
    	$asesor->ap_Materno = $request->ap_Materno;
    	$asesor->numero_tarjeta = $request->numero_tarjeta;
    	$asesor->escolaridad = $request->escolaridad;
    	$asesor->certificacion = $request->certificacion;
    	$asesor->descripcion = $request->descripcion;
        $asesor->numero_tel = $request->numero_tel;
    	$asesor->correo = $request->correo;
    	$asesor->contraseña = bcrypt($request->contraseña);   
    	$asesor->save(); 

    	return response()->json(['mensaje' => 'Datos actualizados con éxito'], 200);
    }

}
