<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Administrador extends Model
{
    //
    protected $fillable =[
        
        'nombre',
        'contraseña',
        'correo',
    ];
    public function asesorados(){
        return $this->belongsToMany('App\Administrador','asesor_administrador');
    }
}
