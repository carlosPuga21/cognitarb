<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asesorado extends Model
{
    //
    protected $fillable =[
        
        'nombre',
        'ap_Paterno',
        'ap_Materno',
        'comprobante_pago',
        'correo',
        'contraseña',

    ];
    public function asesorados(){
        return $this->belongsToMany('App\Asesorado','asesor_asesorado');
    }
}
