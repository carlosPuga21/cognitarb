<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asesor extends Model
{
    //
    protected $fillable =[
        'numero_tel',
        'nombre',
        'ap_Paterno',
        'ap_Materno',
        'numero_tarjeta',
        'escolaridad',
        'certificacion',
        'descripcion',
        'correo',
        'contraseña',
    ];
    public function asesorados(){
        return $this->belongsToMany('App\Asesor','asesor_asesorado');
    }
}
